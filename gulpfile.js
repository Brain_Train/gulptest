let { src, dest } = require('gulp');
let gulp = require('gulp');
let browsersync = require('browser-sync').create();
let scss =  require('gulp-sass')(require('sass'));
let minify = require('gulp-minify-css');
let uglify = require('gulp-uglify-es').default;
let typeScript = require('gulp-typescript');

let progect_folder = 'dist';
let source_folder = 'src';

let path = {
    build: {
        html: progect_folder + '/',
        style: progect_folder + '/style/',
        js: progect_folder + '/ts/',
    },
    src: {
        html: source_folder + '/*.html',
        style: source_folder + '/style/style.scss', 
        ts: source_folder + '/ts/**/*.ts',
    },
    watch: {
        html: source_folder + '/**/*.html',
        style: source_folder + '/style/**/*.scss', 
        ts: source_folder + '/ts/**/*.ts',
    },
    clean: './' + progect_folder + '/'
}

function browserSync() {
    browsersync.init({
        server: {
            baseDir: './' + progect_folder + '/'
        },
        port: 3000,
        notify: false
    })
}

function html() { 
    return src(path.src.html)
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream())
}

function style() {
    return src(path.src.style)
        .pipe(scss({
                outputStyle: 'expanded'
        }))
        .pipe(minify({
            keepBreaks: true
        }))
        .pipe(dest(path.build.style))
        .pipe(browsersync.stream())
}

function watchFiles() {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.style], style);
    gulp.watch([path.watch.ts], ts);
}

function ts() {
    return src(path.src.ts)
        .pipe(typeScript())
        .pipe(uglify())
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}

let watch = gulp.parallel(html, style, ts, watchFiles, browserSync);

exports.default = watch;
