import { employerArr, IEmployeers } from '../data/employyer';
import { restorauntResult } from './calculation';

class Render {
    restorauntData: IEmployeers[];
    modalSwitch: boolean | null;
    buttonOpenModal: HTMLElement;
    allCard: HTMLElement;
    modal: HTMLElement;
    modalCard: HTMLElement;
    information: HTMLElement;
    constructor(data: IEmployeers[]) {
        this.modalSwitch = null;
        this.restorauntData = data;
        this.buttonOpenModal = document.querySelector('.apperButton') as HTMLElement;
        this.allCard = document.querySelector('.blockCard') as HTMLElement;
        this.modal = document.querySelector('.modal') as HTMLElement;
        this.modalCard = document.querySelector('.modal__modalCard') as HTMLElement;
        this.information = document.querySelector('.allInfo') as HTMLElement;
        this.renderCard();
        this.informationRestoraunt();
    }

    informationRestoraunt() {
        let salaryesData = restorauntResult.countingSalariesEachDepartment();
        let avarageSalaryesData = restorauntResult.countingAverageSalaryDepartment();
        let postFired = restorauntResult.menedgerFired(('Meneger'));
        let isFired = restorauntResult.firedAmount(true);
        
        const allSalaryInformation = document.createElement('div');
        allSalaryInformation.className = 'allSalary';

        const avarageInformation = document.createElement('div');
        avarageInformation.className = 'avarageInformation';

        const firedMenegerAmount = document.createElement('div');
        firedMenegerAmount.className = 'firedMeneger';

        const firedEmployyer = document.createElement('div');
        firedEmployyer.className = 'firedEmployyer';

        for ( let i in salaryesData ) {
            allSalaryInformation.innerHTML += `
            <p class="information"> ${i}: ${salaryesData[i]} </p>
            `;
        }

        for ( let i in avarageSalaryesData ) {
            avarageInformation.innerHTML += `
            <p class="information"> ${i}: ${avarageSalaryesData[i]} </p>
            `;
        }

        for ( let i in postFired ) {
            firedMenegerAmount.innerHTML += `
            <p class="information"> ${i}: ${postFired[i]} </p>
            `;
        }

        for ( let i in isFired ) {
            firedEmployyer.innerHTML += `
            <p class="information"> ${i}: ${isFired[i]} </p>
            `;
        }
        
        this.information?.append(avarageInformation);
        this.information?.append(allSalaryInformation);
        this.information?.append(firedMenegerAmount);
        this.information?.append(firedEmployyer);
    }

    renderCards(data: IEmployeers, index: number) {        
        const card: HTMLDivElement = document.createElement('div');
        card.className = 'card';
        card.innerHTML = `
        <h3 class="allLable">${data.name} ${data.surname}</h3>
        <p class="infText"> ${data.department}</p>
        <p class="infText">salary: ${data.salary}$</p>
        <p class="infText">post: ${data.post}</p>
        <p class="infText">status: ${data.fired ? 'fired' : 'working'}</p>
        `;
  
        const buttonChange: HTMLButtonElement = document.createElement('button');
        buttonChange.className = 'cardButton';
        buttonChange.innerHTML = 'Change';
        card.appendChild(buttonChange);
        buttonChange.addEventListener('click', this.openChangingModal.bind(this, data, index));

        this.buttonOpenModal?.addEventListener('click', this.openCreatingModal.bind(this, data));

        const buttonDelete: HTMLButtonElement = document.createElement('button');
        buttonDelete.className = 'cardButtonDelete';
        buttonDelete.innerHTML = 'Delete';
        card.appendChild(buttonDelete);
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));

        return card;
    }

    openModal(data: IEmployeers, index: number) {
        this.modal?.classList.add('active');
        this.modalCard!.innerHTML = `
        <div class="changes">
            <p> Name </p>
            <input class="inputChanges" type="text" placeholder="Enter name..." name="name"> 
        </div>
        <div class="changes">
            <p> Surname </p>
            <input class="inputChanges" type="text" placeholder="Enter surname..." name="surname"> 
        </div>
        <div class="changes">
            <p> Department </p>
            <input class="inputChanges" type="text" placeholder="Enter department..." name="department"> 
        </div>
        <div class="changes">
            <p> Salary </p>
            <input class="inputChanges" type="text" placeholder="Enter salary..." name="salary"> 
        </div>
        <div class="changes">
            <p> Post </p>
            <input class="inputChanges" type="text" placeholder="Enter post..." name="post"> 
        </div>
        <div class="changes">
            <p> Fired </p>
            <input class="check" type="checkbox" placeholder="Enter fired..." name="fired"> 
        </div>
        `;

        const divForApplyButton: HTMLDivElement = document.createElement('div');
        divForApplyButton.className = 'applyButton';
        this.modalCard!.append(divForApplyButton);

        const button: HTMLButtonElement = document.createElement('button');
        button.className = 'editeEmploeerButton';
        button.innerText = 'Apply changes';
        divForApplyButton.appendChild(button);

        divForApplyButton.addEventListener('click', this.editeEmployeer.bind(this, index));

        this.modalCard!.addEventListener('click', this.stopPropagation.bind(this));
        this.modal?.addEventListener('click', this.closeModal.bind(this));
    }

    createUser(event: any) {
        event.preventDefault();

        let newEmpoloyeer = new FormData(event.target.closest('form'));  
        
        this.restorauntData.unshift({
            department: String(newEmpoloyeer.get('department')) || 'Please enter department',
            name: String(newEmpoloyeer.get('name')) || 'Name',
            surname: String(newEmpoloyeer.get('surname')) || 'Surname',
            salary: Number(newEmpoloyeer.get('salary')) || 0,
            post: String(newEmpoloyeer.get('post')) || 'Change post',
            fired: Boolean(newEmpoloyeer.get('fired')) || false,
        });

        this.renderCard();
    
    }

    renderCard() {
        this.allCard!.innerHTML = ' ';
        const cards = this.restorauntData.map((data, index) => this.renderCards(data, index));
        this.allCard!.append(...cards); 
    }

    changeUsers(event: any, i: number,) {
        event.preventDefault();

        let changeEmployeer = new FormData(event.target.closest('form'));
        
        this.restorauntData[i].name =  String(changeEmployeer.get('name')) || employerArr[i].name; 
        this.restorauntData[i].surname =  String(changeEmployeer.get('surname')) || employerArr[i].surname;
        this.restorauntData[i].department =  String(changeEmployeer.get('department')) || employerArr[i].department;
        this.restorauntData[i].salary =  Number(changeEmployeer.get('salary')) || employerArr[i].salary;
        this.restorauntData[i].post =  String(changeEmployeer.get('post')) || employerArr[i].post;
        this.restorauntData[i].fired =  Boolean(changeEmployeer.get('fired')) || employerArr[i].fired;

        this.renderCard();
        
    }

    deleteUsers(index: number) {
        this.restorauntData.splice(index, 1);
        this.renderCard();
    }

    closeModal() {
        this.modal?.classList.remove('active');
    }

    openChangingModal(data: IEmployeers, index: number) {
        this.modalSwitch = true;
        this.openModal(data, index);
    }

    editeEmployeer(index: number, event: any) {
        this.information!.innerHTML = ' ';
        if(this.modalSwitch === true){
            this.changeUsers(event, index);
        } else if(this.modalSwitch === false) {
            this.createUser(event);
        }
        this.closeModal();
        this.informationRestoraunt();
    }

    openCreatingModal(data: IEmployeers, index: any) {
        this.modalSwitch = false;
        this.openModal(data, index);
    }

    stopPropagation(element: any) {
        element.stopPropagation();
    }

}

const render = new Render(employerArr);


