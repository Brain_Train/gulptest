import { bankUsers, IBank } from '../data/employyer';

class BankCalculation {
    bankData: IBank[];
    constructor(data: IBank[]) {
        this.bankData = data;
    }

    fetchData(data: Record<string, number>, callback: Function) {
        return fetch('https://www.cbr-xml-daily.ru/daily_json.js').then(function (result) {
            return result.json();
        }).then(function (currencyArray) {
            let currency: Record<string, number> = {};
            
            currency.USD = currencyArray.Valute.USD.Previous;
            currency.EUR = currencyArray.Valute.EUR.Previous;
            currency.UAH = currencyArray.Valute.UAH.Previous;
            return callback(data, currency);
        });
    }

    countingMoney() {
        let allMoney: Record<string, number> = {};

        this.bankData.forEach((value) => {
            allMoney[value.credit.ownBalance.currency] = allMoney[value.credit.ownBalance.currency] || 0;
            allMoney[value.credit.ownBalance.currency] += value.credit.ownBalance.ownBalance;
            
            allMoney[value.credit.creditBalance.currency] = allMoney[value.credit.ownBalance.currency] || 0;
            allMoney[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;

            allMoney[value.debit.curentBalance] = allMoney[value.debit.curentBalance] || 0;
            allMoney[value.debit.curentBalance] += value.debit.curentBalance;
        });
        
        return this.fetchData(allMoney, (data: Record<string, number>, currency: number[]) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;

            for ( let index in currency ) {
                for ( let compareIndex in allMoney ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += allMoney[compareIndex] * currency[index];
                    }
                }
            }

            return Math.round(convertAllMoney);
        });
    }

    countingBorowedMoney() {
        let borrowedMoney: Record<string, number> = {};

        this.bankData.forEach((value) => {
            borrowedMoney[value.credit.creditBalance.currency] = borrowedMoney[value.credit.creditBalance.currency] || 0;
            borrowedMoney[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;
        });

        return this.fetchData(borrowedMoney, (data: Record<string, number>, currency: number[]) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;

            for ( let index in currency ) {
                for ( let compareIndex in borrowedMoney ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += borrowedMoney[compareIndex] * currency[index];
                    }
                }
            }

            return Math.round(convertAllMoney);
        });
    }


    countingBorowedMonyeInaciveUsers() {
        let inactive: Record<string, number> = {};

        this.bankData.forEach((value) => {
            inactive[value.credit.creditBalance.currency] = inactive[value.credit.creditBalance.currency] || 0;

            if ( value.activeUser === false ) {
                inactive[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;
            }
        });
        
        return this.fetchData(inactive, (data: Record<string, number>, currency: number[]) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;
            
            for ( let index in currency ) {
                for ( let compareIndex in inactive ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += inactive[compareIndex] * currency[index];
                    }
                }
            }

            return Math.round(convertAllMoney);
        });
    } 
}

export const bankResult = new BankCalculation(bankUsers);
